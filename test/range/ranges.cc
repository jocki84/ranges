#include "rng.h"

#include <vector>

#define BOOST_TEST_MODULE ranges test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using rng::Take;
using rng::all;
using rng::as;
using rng::enumerate;
using rng::nest;
using rng::primes;
using rng::size;
using rng::zip;

BOOST_AUTO_TEST_CASE(test_all)
{
    std::vector const expected{1, 2, 3};
    BOOST_TEST(expected == (all({1, 2, 3}) | as<decltype(expected)>));
}

BOOST_AUTO_TEST_CASE(test_all_unequal)
{
    std::vector const expected{1, 2, 4};
    BOOST_TEST(expected != (all({1, 2, 3}) | as<decltype(expected)>));
}

BOOST_AUTO_TEST_CASE(test_enumerate)
{
    auto const r0 = all({"alpha", "beta", "gamma"});
    std::vector const expected{std::pair{0, "alpha"}, std::pair{1, "beta"}, std::pair{2, "gamma"}};
    BOOST_TEST(expected == (enumerate(r0) | as<decltype(expected)>));
}

BOOST_AUTO_TEST_CASE(test_zip)
{
    auto const r0 = all({1, 2, 3});
    auto const r1 = all({'a', 'b', 'c'});
    std::vector const expected{std::pair{1, 'a'}, std::pair{2, 'b'}, std::pair{3, 'c'}};
    BOOST_TEST(expected == (zip(r0, r1) | as<decltype(expected)>));
}

BOOST_AUTO_TEST_CASE(test_nest)
{
    auto const r0 = all({"alpha", "beta"});
    auto const r1 = all({1, 2, 3});
    auto const r2 = all({'a', 'b'});
    BOOST_TEST((nest(r0, r1, r2) | size) == r0.size() * r1.size() * r2.size());

    std::vector const expected{
        std::make_tuple("alpha", 1, 'a'),
        std::make_tuple("beta", 1, 'a'),
        std::make_tuple("alpha", 2, 'a'),
        std::make_tuple("beta", 2, 'a'),
        std::make_tuple("alpha", 3, 'a'),
        std::make_tuple("beta", 3, 'a'),
        std::make_tuple("alpha", 1, 'b'),
        std::make_tuple("beta", 1, 'b'),
        std::make_tuple("alpha", 2, 'b'),
        std::make_tuple("beta", 2, 'b'),
        std::make_tuple("alpha", 3, 'b'),
        std::make_tuple("beta", 3, 'b'),
    };
    BOOST_TEST(expected == (nest(r0, r1, r2) | as<decltype(expected)>));
}

BOOST_AUTO_TEST_CASE(test_mutation)
{
    std::vector<int> v0 = {1, 2, 3, 4, 5};
    for (auto rng = enumerate(all(v0) | Take{3}); !rng.empty(); rng.popFront()) {
            // Multiply each element by its index.
            std::get<1>(rng.front()) *= std::get<0>(rng.front());
        }

    std::vector const expected{0, 2, 6, 4, 5};
    BOOST_TEST(expected == v0);
}

BOOST_AUTO_TEST_CASE(test_primes)
{
    std::vector const expected{2, 3, 5, 7, 11, 13, 17, 19, 23};
    BOOST_TEST(expected == (primes() | Take{expected.size()} | as<decltype(expected)>));
}
