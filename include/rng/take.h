#ifndef RNG_TAKE_H
#define RNG_TAKE_H

namespace rng
{
    template<typename Range>
    class TakeRange
    {
    public:
        TakeRange(Range range, std::size_t const length)
        : mRange{std::move(range)}
        , mLength{length}
        {}

        constexpr bool empty() const
        {
            return 0 == mLength || mRange.empty();
        }

        constexpr decltype(auto) front() const
        {
            return mRange.front();
        }

        constexpr void popFront()
        {
            mRange.popFront();
            mLength -= 1;
        }

    private:
        Range mRange;
        std::size_t mLength;
    };

    struct Take
    {
        template<typename Range>
        friend constexpr auto operator |(Range range, Take const& take)
        {
            return TakeRange{range, take.length};
        }

        std::size_t length;
    };
} // namespace rng

#endif // RNG_TAKE_H
