#ifndef RNG_ZIP_H
#define RNG_ZIP_H

#include "tpl/anyOf.h"
#include "tpl/each.h"
#include "tpl/transform.h"

#include <functional>
#include <tuple>
#include <type_traits>
#include <utility>

namespace rng
{
    namespace detail
    {
        /**
         * Wrap a value in a std::reference_wrapper if it is an lvalue.
         */
        template<typename T>
        constexpr auto wrapLvalue(T&& t)
        {
            using is_ref = std::is_lvalue_reference<T>;
            using ref_wrap = std::reference_wrapper<std::remove_reference_t<T>>;

            return typename std::conditional_t<is_ref::value, ref_wrap, T>{t};
        }

        /**
         * A range which wraps a tuple of ranges.
         */
        template<typename... Ranges>
        class ZipRange
        {
        public:
            explicit constexpr ZipRange(Ranges... ranges)
            : mRanges{std::forward<Ranges>(ranges)...}
            {}

            constexpr bool empty() const
            {
                return tpl::anyOf(
                    mRanges,
                    [](auto const& rng) {
                        return rng.empty();
                    }
                );
            }

            constexpr void popFront()
            {
                tpl::each(
                    mRanges,
                    [](auto& rng) {
                       rng.popFront();
                    }
                );
            }

            constexpr auto front() const
            {
                return tpl::transform(
                    mRanges,
                    [](auto const& rng) {
                        return wrapLvalue(rng.front());
                    }
                );
            }

        private:
            std::tuple<std::decay_t<Ranges>...> mRanges;
        };
    }

    template<typename... Ranges>
    constexpr auto zip(Ranges&&... ranges)
    {
        return detail::ZipRange<Ranges...>{std::forward<Ranges>(ranges)...};
    }
}

#endif // RNG_ZIP_H
