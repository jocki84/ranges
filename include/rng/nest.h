#ifndef RNG_NEST_H
#define RNG_NEST_H

#include "tpl/allOf.h"
#include "tpl/anyOf.h"
#include "tpl/transform.h"

#include <array>
#include <tuple>
#include <type_traits>

namespace rng {
    namespace detail {
        template<typename... Ranges>
        class NestedRange {
        public:
            explicit constexpr NestedRange(Ranges... ranges)
            : mRanges{{ranges, ranges}...}
            , mEmpty{}
            {
                mEmpty = tpl::anyOf(
                    mRanges,
                    [](auto const& rng) {
                        return rng[0].empty();
                    }
                );
            }

            constexpr bool empty() const {
                return mEmpty;
            }

            constexpr void popFront() {

// Iteration ends when all ranges have needed to be reset.

                mEmpty = tpl::allOf(
                    mRanges,
                    [](auto& rng) {
                        rng[0].popFront();
                        if (!rng[0].empty()) {
                            return false;
                        }
                        rng[0] = rng[1];
                        return true;
                    }
                );
            }

            constexpr auto front() const {
                return tpl::transform(
                    mRanges,
                    [](auto const& rng) {
                        return rng[0].front();
                    }
                );
            }

        private:
            using Tuple = std::tuple<std::array<std::decay_t<Ranges>, 2>...>;
            Tuple mRanges;
            bool mEmpty;
        };
    }

    template<typename... Ranges>
    constexpr auto nest(Ranges&&... ranges) {
        return detail::NestedRange<std::decay_t<Ranges>...>(
            std::forward<Ranges>(ranges)...
        );
    }
}

#endif // RNG_NEST_H
