#ifndef RNG_FIND_IF_H
#define RNG_FIND_IF_H

namespace rng
{
    template<typename Range, typename Pred>
    Range findIf(Range range, Pred pred)
    {
        for (; !range.empty(); range.popFront()) {
            if (pred(range.front())) {
                return range;
            }
        }
        return range;
    }
} // namespace rng

#endif // RNG_FIND_IF_H
