#ifndef TPL_ALL_OF_H
#define TPL_ALL_OF_H

#include <tuple>

namespace tpl
{
    namespace detail
    {
        template<typename Pred>
        constexpr bool allOfImpl(Pred)
        {
            return true;
        }

        template<typename Pred, typename Arg0, typename ...Tuple>
        constexpr bool allOfImpl(Pred pred, Arg0&& arg0, Tuple&& ...tuple)
        {
            return
                pred(std::forward<Arg0>(arg0)) &&
                allOfImpl(pred, std::forward<Tuple>(tuple)...);
        }
    } // namespace tpl::detail

    template<typename Tuple, typename Pred>
    constexpr bool allOf(Tuple&& tuple, Pred pred)
    {
        return std::apply(
            [&pred](auto&& ...tpl) {
                return detail::allOfImpl(
                    pred,
                    std::forward<decltype(tpl)>(tpl)...
                );
            },
            std::forward<Tuple>(tuple)
        );
    }
} // namespace tpl

#endif // TPL_ALL_OF_H

