#ifndef TPL_ACCUMULATE_H
#define TPL_ACCUMULATE_H

#include <tuple>

namespace tpl
{
    namespace detail
    {

// Returning the reference parameter is unsafe for rvalues. The overload
// below returns those by value.

        template<typename Func, typename Initial>
        constexpr decltype(auto) accumulateImpl(Func, Initial& initial)
        {
            return initial;
        }

        template<typename Func, typename Initial>
        constexpr auto accumulateImpl(Func, Initial const& initial)
        {
            return initial;
        }

        template<typename Func, typename Initial, typename Arg0, typename ...Tuple>
        constexpr decltype(auto) accumulateImpl(
            Func func,
            Initial&& initial,
            Arg0&& arg0,
            Tuple&& ...tuple
        )
        {
            return accumulateImpl(
                func,
                func(std::forward<Initial>(initial), std::forward<Arg0>(arg0)),
                std::forward<Tuple>(tuple)...
            );
        }
    } // namespace tpl::detail

    template<typename Tuple, typename Func>
    constexpr decltype(auto) accumulate(Tuple&& tuple, Func func)
    {
        return std::apply(
            [&func](auto&& ...tpl) {
                return detail::accumulateImpl(func, std::forward<decltype(tpl)>(tpl)...);
            },
            std::forward<Tuple>(tuple)
        );
    }

    template<typename Tuple, typename Initial, typename Func>
    constexpr decltype(auto) accumulate(Tuple&& tuple, Initial&& initial, Func func)
    {
        return std::apply(
            [&func, &initial](auto&& ...tpl) {
                return detail::accumulateImpl(
                    func,
                    std::forward<Initial>(initial),
                    std::forward<decltype(tpl)>(tpl)...
                );
            },
            std::forward<Tuple>(tuple)
        );
    }
} // namespace tpl

#endif // TPL_ACCUMULATE_H
