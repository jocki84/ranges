#ifndef TPL_TRANSFORM_H
#define TPL_TRANSFORM_H

#include <tuple>
#include <utility>

namespace tpl
{
    /**
     * A transform for tuples.
     */
    template<typename Tuple, typename Func>
    constexpr decltype(auto) transform(Tuple&& tuple, Func&& func)
    {
        return std::apply(
            [&func](auto&&... args) {
                return std::make_tuple(
                    std::forward<Func>(func)(
                        std::forward<decltype(args)>(args)
                    )...
                );
            },
            std::forward<Tuple>(tuple)
        );
    }
}

#endif // TPL_TRANSFORM_H
