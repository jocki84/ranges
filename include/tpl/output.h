#ifndef TPL_OUTPUT_H
#define TPL_OUTPUT_H

#include "tpl/each.h"

#include <iosfwd>
#include <tuple>

namespace tpl
{
    /**
     * Tuple output.
     */
    template<typename Char, typename Traits, typename... Types>
    constexpr std::basic_ostream<Char, Traits>& operator <<(
        std::basic_ostream<Char, Traits>& os, std::tuple<Types...> const& tuple
    ) {
        os << '(';
        char const* sep{""};
        each(
            tuple,
            [&sep, &os](auto const& v) {
                os << sep << v;
                sep = ", ";
            }
        );
        return os << ')';
    }
}

#endif // TPL_OUTPUT_H
