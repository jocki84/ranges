# Allow user to specify build mode, include and library paths.
vars = Variables()
vars.AddVariables(
    PathVariable(
        "CPPPATH",
        "C/C++ header search path.",
        None,
        PathVariable.PathIsDir
    ),

    PathVariable(
        "LIBPATH",
        "Library search path.",
        None,
        PathVariable.PathIsDir
    ),

    ListVariable(
        "MODES",
        "The build mode.",
        "release",
        ["debug", "release"]
    ),
)

env = Environment(variables=vars)
Help(vars.GenerateHelpText(env), append=True)
if vars.UnknownVariables():
    print("Unknown command line variables: %s" % (vars.UnknownVariables()))
    Exit(64) # EX_USAGE

# Common configuration.
env.Append(
    CCFLAGS=["-Wall", "-g", "-pedantic"],
    CPPPATH=["#include"],
    CXXFLAGS=["-std=c++17"],
)

if not (GetOption("clean") or GetOption("help")):
    config = Configure(env)

    # Find boost.
    if not config.CheckCXXHeader("boost/version.hpp"):
        print("Specify boost installation path using --cpppath and --libpath.")
        Exit(1)

    # Check for boost unit test framework.
    if not config.CheckLibWithHeader("boost_unit_test_framework", "boost/test/unit_test.hpp", "c++"):
        print("Specify boost installation path using --cpppath and --libpath.")
        Exit(1)

    env = config.Finish()

# Set up configuration-specific environments.
envs = {config: env.Clone() for config in ("debug", "release")}

envs["debug"].Append(
    CCFLAGS=["-O0"],
)

envs["release"].Append(
    CCFLAGS=["-O"],
    CPPDEFINES=["NDEBUG"],
)

# Build each configuration.
for config, env in envs.items():
    if config not in env["MODES"]:
        continue

    env.SConscript(
        dirs=["."],
        duplicate=0,
        exports=["env"],
        variant_dir=".obj/$PLATFORM/%s" % (config,),
    )
